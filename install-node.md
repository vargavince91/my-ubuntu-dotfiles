# Install Node.js

Install Node.js with [NVM](https://github.com/creationix/nvm). You find the instructions for Ubuntu 16.04:

1. You need to get curl or wget to download the `.sh` file. `sudo apt install curl`
2. Download the latest `.sh` file: `curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.1/install.sh | bash`
3. After the installation is finished, run `source ~/.bashrc`
4. Verify it's downloaded with `command -v nvm`
5. Go to [`nodejs.org`](https://nodejs.org/) to see latest version
6. Install a version using `nvm install 7.5.0` and set it as default `nvm alias default 7.5.0`.
7. Verify node is available `node -v`

